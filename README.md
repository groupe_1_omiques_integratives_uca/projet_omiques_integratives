# Projet_Omiques_intégratives

Master 2 Bioinformatique de l'Université Clermont Auvergne - Unité d'Enseignement "Omique intégratives"

## Objectifs

Les objectifs de cette etude sont d'anlyser des resultats de RRBS, DNase et RNA-seq en ayant une approche multi-omique.
Les données initiales sont issus de l'article:  
Gomez-Cabrero et al. (2019) : STATegra, a compréhensive multi-omics dataset of B-cell différentiation in mouse, Scient. Data 6:256 (doi.org/10.1038/s41597-019-0202-7)[1](https://www.nature.com/articles/s41597-019-0202-7)

Nous allons nous focaliser sur 3 réplicats biologiques pour les temps T0 et T18.

## Répertoire initial

Le répertoire initial doit rester identique au répertoire projet_omiques_integratives/


## Script
Pour lancer les scripts, le chemin absolue doivent être ajouter. 
Nos scripts sont focaliser sur le prés traitement des données RNA-seq et l'intégration au autres types de données qui s'en suit.

## Data 
Le dossier /data contiens les données initiales et intérmédiaires de cette analyse 

## Resultats
Le dossier /results contiens les resulats de nos alanyses comme les listes de gènes generé et les analyses GSEA et DESeq2
