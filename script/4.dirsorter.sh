#!/bin/bash

conda activate ikaros
#Placer les données trimmé dans les dossiers control_0 control_24 ikaros_0 ikaros_24

control18=(
"SRR2961355"
"SRR2961356"
"SRR2961357"
"SRR2961358"
"SRR2961359"
"SRR2961360")

control0=("SRR2961330"
"SRR2961331"
"SRR2961332"
"SRR2961333"
"SRR2961334"
"SRR2961335")

ikaros0=("SRR2961367"
"SRR2961368"
"SRR2961369"
"SRR2961370"
"SRR2961371"
"SRR2961372")

ikaros18=("SRR2961391"
"SRR2961392"
"SRR2961393"
"SRR2961394"
"SRR2961395"
"SRR2961396")

mkdir projet_omiques_integratives/results/trimmed/control0
mkdir projet_omiques_integratives/results/trimmed/control18
mkdir projet_omiques_integratives/results/trimmed/ikaros0
mkdir projet_omiques_integratives/results/trimmed/ikaros18

for i in ${control0[@]} ; do mv projet_omiques_integratives/results/trimmed/*$i*trimmed.paired.fastq.gz projet_omiques_integratives/results/trimmed/contol0;done
for i in ${control18[@]} ; do mv projet_omiques_integratives/results/trimmed/*$i*trimmed.paired.fastq.gz projet_omiques_integratives/results/trimmed/control18 ;done
for i in ${ikaros0[@]} ; do mv projet_omiques_integratives/results/trimmed/*$i*trimmed.paired.fastq.gz projet_omiques_integratives/results/trimmed/ikaros0 ;done
for i in ${ikaros18[@]} ; do mv projet_omiques_integratives/results/trimmed/*$i*trimmed.paired.fastq.gz projet_omiques_integratives/results/trimmed/ikaros18 ;done

