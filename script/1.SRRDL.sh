#!/bin/bash

conda activate ikaros

SRR=("SRR2961355"
    "SRR2961356"
    "SRR2961357"
    "SRR2961358"
    "SRR2961359"
    "SRR2961360"
    "SRR2961330"
    "SRR2961331"
    "SRR2961332"
    "SRR2961333"
    "SRR2961334"
    "SRR2961335"
    "SRR2961367"
    "SRR2961372"
    "SRR2961371"
    "SRR2961370"
    "SRR2961369"
    "SRR2961368"
    "SRR2961391"
    "SRR2961396"
    "SRR2961395"
    "SRR2961394"
    "SRR2961393"
    "SRR2961392")

INPUT=projet_omiques_integratives/data/

#Telechargement des données
for i in ${SRR[@]}; do fastq-dump.3.0.7 --split-3 -O $INPUT --gzip $i; done

