#!/bin/bash

conda activate ikaros
#Controle qualité des données de départ
mkdir projet_omiques_integratives/results/fastqc-init

INPUT=projet_omiques_integratives/data/*.fastq.gz
OUTPUT=projet_omiques_integratives/results/fastqc-init/

fastqc -t 6 -o $OUTPUT $INPUT

conda deactivate

conda activate multiqc
#Multiqc
mkdir projet_omiques_integratives/results/fastqc-init/multiqc-init

INPUT=projet_omiques_integratives/results/fastqc-init/
OUTPUT=projet_omiques_integratives/results/fastqc-init/multiqc-init

multiqc -o $OUTPUT $INPUT
