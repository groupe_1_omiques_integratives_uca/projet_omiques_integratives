#!/bin/bash

#activation de l'env
conda activate ikaros

#mise en place des variables DNase-seq
control0=("SRR2960359" "SRR2960360" "SRR2960361")
control18=("SRR2960371" "SRR2960372" "SRR2960373")
ikaros0=("SRR2960379" "SRR2960379" "SRR2960380")
ikaros18=("SRR2960379" "SRR2960379" "SRR2960380")

cond=("control0" "control18" "ikaros0" "ikaros18")


#Analyse des données DNase
## Récuperer les régions de ouvert par la DNase avec des sites de fixation d'IKAROS présent dedans
INPUT=projet_omiques_integratives/results/dnase/Peak-Calling
OUTPUT=projet_omiques_integratives/results/dnase/motif_intersect
motif=projet_omiques_integratives/data/lft_BDikzmm39.bed

for i in "${cond[@]}"; 
do file1=("${i}[0]") file2=("${i}[1]") file3=("${i}[2]")
  bedtools intersect -wo -a "$motif" -b "$INPUT"/"${i}"/"${!file1}" "$INPUT"/"${i}"/"${!file2}" "$INPUT"/"${i}"/"${!file3}" -name "${!file1}" "${!file2}" "${!file3}" > "$OUTPUT"/"${i}"/narrowPeak_"${i}".bed 
  cat "$OUTPUT"/"${i}"/narrowPeak_"${i}".bed | awk '{OFS="\t"; print $7, $8-1000, $9+1000, $10, $17}'> "$OUTPUT"/"${i}"/fix_site_"${i}"_1000.bed
   done

## Récuperer une liste de gène présent 
INPUT=projet_omiques_integratives/results/dnase/motif_intersect
OUTPUT=projet_omiques_integratives/results/dnase/gene_narrow
gene_list=projet_omiques_integratives/data/gene_list_TSS.bed

for i in "${cond[@]}"
 do file1=("${i}[0]") file2=("${i}[1]") file3=("${i}[2]")
  bedtools intersect -wo -a "$gene_list" -b "$INPUT"/"${i}"/fix_site_"${i}"_1000.bed| awk '{OFS="\t"; print $1,$2,$3,$4}' > "$INPUT"/"${i}"/gene_narrow_"${i}".bed
   cat "$OUTPUT"/"${i}"/gene_narrow_"${i}".bed| cut -f4 |sort | uniq > "$OUTPUT"/"${i}"/list_gene_"${i}".txt

## liste de gène différentiellement exprimé au alentour des sites de fixation d'ikaros ouvert a l'aide de diagramme VENN
