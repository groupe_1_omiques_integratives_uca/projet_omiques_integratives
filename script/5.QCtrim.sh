#!/bin/bash

conda activate ikaros
#Controle qualité des données après trimming
mkdir projet_omiques_integratives/results/fastqc-post

INPUT=projet_omiques_integratives/results/trimmed/*/*.fastq.gz
OUTPUT=projet_omiques_integratives/results/fastqc-post/

fastqc -t 6 -o $OUTPUT $INPUT

conda deactivate
conda activate multiqc
#Multiqc
mkdir projet_omiques_integratives/results/fastqc-post/multiqc-post
INPUT=projet_omiques_integratives/results/fastqc-post/
OUTPUT=projet_omiques_integratives/results/fastqc-post/multiqc-post

multiqc -o $OUTPUT $INPUT
