#!/bin/bash
conda activate ikaros

mkdir projet_omiques_integratives/results/quantif
FILES=($(find "projet_omiques_integratives/results/trimmed/*/*" -type f -name "*.sam.sorted.bam"))

#Quantification du niveau génétique
htseq-count -s reverse -a 20 -m union -q <PATH_out>/<name_sample>_ready.sam genes.gtf > <PATH_out>/<name_sample>.htseq.union.gtf

for i in ${FILES[@]};do htseq-count -s reverse -a 20 -m union -q  $i home/RNA-seq/data/reference/MMgrcm39/ncbi_dataset/data/GCF_000001635.27/genomic.gff > projet_omiques_integratives/results/quantif/$(basename $i).htseq.gtf

