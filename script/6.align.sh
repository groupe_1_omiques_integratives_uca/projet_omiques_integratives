#!/bin/bash

conda activate ikaros
cond=("control0"
"control18"
"ikaros0"
"ikaros18")

#Alignement des données pour chaque donnée control_0 control_24 ikaros_0 ikaros_24
#par exemple pour control_0

for i in ${cond[@]} ;do mkdir projet_omiques_integratives/results/align/$i

for cond in ${cond[@]}; do for fc in projet_omiques_integratives/results/trimmed/$cond/*1.trimmed.paired.fastq.gz; do name=$(basename $fc| cut -d "." -f1 | cut -d "_" -f1| uniq); \
dir=$(dirname "$fc") ; STAR --runThreadN 24\
 --genomeDir /home/RNA-seq/MMgrcm39/ncbi_dataset/data/index_STAR\
  --readFilesIn $dir"/"$name"_1.trimmed.paired.fastq.gz" $dir"/"$name"_2.trimmed.paired.fastq.gz"\
   --outFileNamePrefix projet_omiques_integratives/results/align/${name}\
    --readFilesCommand gunzip -c\
     --outSAMtype BAM SortedByCoordinate;done; done
