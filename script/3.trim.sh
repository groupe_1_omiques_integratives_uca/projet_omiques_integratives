#!/bin/bash

conda activate ikaros
mkdir projet_omiques_integratives/results/trimmed

TRIM=/home/ubuntu/Software/Trimmomatic-0.39/trimmomatic-0.39.jar

for i in projet_omiques_integratives/data/*1.fastq.gz; do basename=$(echo $i | cut -d "." -f1 | cut -d "_" -f1);name_1="${basename}_1.fastq.gz"; name_2="${basename}_2.fastq.gz";\ java -jar $TRIM PE -threads 6  -phred33 $name_1 $name_2 projet_omiques_integratives/results/trimmed/${basename}_1.trimmed.paired.fastq.gz projet_omiques_integratives/results/trimmed/${basename}_1.trimmed.unpaired.fastq.gz projet_omiques_integratives/results/trimmed/${basename}_2.trimmed.paired.fastq.gz projet_omiques_integratives/results/trimmed/${basename}_2.trimmed.unpaired.fastq.gz\ ILLUMINACLIP:/home/ubuntu/Software/Trimmomatic-0.39/adapters/TruSeq3-PE-2.fa:2:30:10 LEADING:3 TRAILING:3 MINLEN:25 SLIDINGWINDOW:4:15; done
